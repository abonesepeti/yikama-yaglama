<?php include 'layout/header.php'; ?>
<?php include 'layout/navbar.php'; ?>
    <script>
        $(document).ready(function () {
            var table = $('#datatable').DataTable({
                'ajax': 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=customer',
                'language': {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Turkish.json'
                },
                "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<div class="btn-group"><button class="btn-xs btn btn-warning" data-toggle="modal" data-target=".edit-modal" data-type="edit"><i class="fa fa-edit"></i> Düzenle</button><button class="btn-xs btn btn-danger" data-toggle="modal" data-target=".remove-modal" data-type="remove"><i class="fa fa-trash-alt"></i> Sil</button></div>'
                }]
            });

            var dataID;
            $('#datatable tbody').on('click', 'button', function () {
                var data = table.row($(this).parents('tr')).data();
                dataID = data[4];

                if ($(this).data('type') == 'edit') {
                    $.ajax({
                        url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=post',
                        type: 'POST',
                        data: {id:dataID,type:'customer-edit-list'},
                        success: function (result) {
                            $('[name="name_surname"]').val(result['name_surname']);
                            $('[name="gsm"]').val(result['gsm']);
                            $('[name="plaka"]').val(result['plaka']);
                        }
                    })
                } else if ($(this).data('type') == 'remove') {
                    $('[data-type="customerMessage"]').html('<b>' + data[0] + '</b> isimli müşteriyi silmek istediğinize emin misiniz?');
                }
            });


            $('[data-type="removeSuccess"]').on('click', function () {
                $.ajax({
                    url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=post',
                    type: 'POST',
                    data: {id: dataID, type: 'customer-remove'},
                    success: function (result) {
                        if (result['code'] == 200) {
                            $('.remove-modal').modal('toggle');
                            table.ajax.reload();
                        }
                    }
                });
            });


            $('[data-type="editSuccess"]').on('click', function () {
                $.ajax({
                    url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=post',
                    type: 'POST',
                    data: {id:dataID,type:'customer-edit',name_surname:$('[name="name_surname"]').val(),gsm:$('[name="gsm"]').val(),plaka:$('[name="plaka"]').val()},
                    success: function (result) {
                        if(result['code'] == 200)
                        {
                            $('.edit-modal').modal('toggle');
                            table.ajax.reload();
                        }
                    }
                });
            });

            $('[data-type="customerAdd"]').on('click', function () {
                $.ajax({
                    url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=post',
                    type: 'POST',
                    data: {type:'customer-add',name_surname:$('[name="new_name_surname"]').val(),gsm:$('[name="new_gsm"]').val(),plaka:$('[name="new_plaka"]').val()},
                    success: function (result) {
                        if(result['code'] == 200)
                        {
                            $('.new-modal').modal('toggle');
                            table.ajax.reload();
                        }
                    }
                });
            });
        });
    </script>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-12"><h4 class="page-title m-0">Müşteri Yönetimi <button type="button" data-toggle="modal" data-target=".new-modal" data-type="customerAddModal" class="btn btn-success btn-sm">Müşteri Ekle <i class="fa fa-plus"></i></button></h4></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Adı Soyadı</th>
                                    <th>Plaka</th>
                                    <th>GSM</th>
                                    <th>Kayıt Tarihi</th>
                                    <th>İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade remove-modal" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Müşteri Silme</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" data-type="customerMessage">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-type="removeSuccess"><i class="fa fa-check"></i>
                        Kabul Et
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>
                        Vazgeç
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade edit-modal" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Müşteri Düzenleme</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Adı Soyadı</label>
                        <input type="text" class="form-control" name="name_surname">
                    </div>
                    <div class="form-group">
                        <label>GSM Numarası</label>
                        <input type="text" class="form-control" name="gsm">
                    </div>
                    <div class="form-group">
                        <label>Plaka</label>
                        <input type="text" class="form-control" name="plaka">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-type="editSuccess"><i class="fa fa-check"></i>
                        Kabul Et
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>
                        Vazgeç
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade new-modal" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Müşteri Ekle</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Adı Soyadı</label>
                        <input type="text" class="form-control" name="new_name_surname">
                    </div>
                    <div class="form-group">
                        <label>GSM Numarası</label>
                        <input type="text" class="form-control" name="new_gsm">
                    </div>
                    <div class="form-group">
                        <label>Plaka</label>
                        <input type="text" class="form-control" name="new_plaka">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-type="customerAdd"><i class="fa fa-save"></i>
                        Kaydet
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>
                        Vazgeç
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php include 'layout/footer.php'; ?>