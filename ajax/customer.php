<?php
header('Content-Type: application/json');

include 'system/System.php';

$dataList = [];
$query = $adapter->query("SELECT * FROM customer", PDO::FETCH_ASSOC);
if ( $query->rowCount() ){
    foreach( $query as $row ){
        $dataList['aaData'][] = [
            $row['name_surname'],
            $row['plaka'],
            $row['gsm'],
            $row['create_date'],
            $row['id']
        ];
    }
}

print_r(json_encode($dataList));