<?php
header('Content-Type: application/json');

include 'system/System.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $query = $adapter->prepare("INSERT INTO actions SET
personal_id = ?,
customer_id = ?,
price = ?,
percent = ?,
status = ?,
note = ?,
`date` = ?");
    $result = $query->execute([
        $_POST['personal_id'],
        $_POST['customer_id'],
        $_POST['price'],
        $_POST['percent'],
        'P',
        $_POST['note'],
        date('Y-m-d'),
    ]);

    if($result)
    {
        print_r(json_encode([
            'code' => 200
        ]));
    }else{
        print_r(json_encode([
            'code' => 201
        ]));
    }
}