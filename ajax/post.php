<?php
header('Content-Type: application/json');

include 'system/System.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /** MÜŞTERİ */
    if($_POST['type'] == 'customer-remove')
    {
        $result = $adapter->query('DELETE FROM customer WHERE id='.$_POST['id']);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }elseif($_POST['type'] == 'customer-edit-list')
    {
        $result = $adapter->query('SELECT * FROM customer WHERE id='.$_POST['id']);
        foreach( $result as $row ){
            print_r(json_encode([
                'name_surname' => $row['name_surname'],
                'gsm' => $row['gsm'],
                'plaka' => $row['plaka'],
            ]));
        }
    }elseif($_POST['type'] == 'customer-edit')
    {
        $result = $adapter->query('UPDATE customer SET name_surname="'.$_POST['name_surname'].'", gsm="'.$_POST['gsm'].'", plaka="'.$_POST['plaka'].'" WHERE id='.$_POST['id']);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }elseif($_POST['type'] == 'customer-add')
    {
        $query = $adapter->prepare("INSERT INTO customer SET
name_surname = ?,
gsm = ?,
plaka = ?,
create_date = ?");
        $result = $query->execute([
            $_POST['name_surname'],
            $_POST['gsm'],
            $_POST['plaka'],
            date('Y-m-d'),
        ]);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }
    /** PERSONEL */
    if($_POST['type'] == 'user-remove')
    {
        $result = $adapter->query('DELETE FROM personal WHERE id='.$_POST['id']);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }elseif($_POST['type'] == 'user-edit-list')
    {
        $result = $adapter->query('SELECT * FROM personal WHERE id='.$_POST['id']);
        foreach( $result as $row ){
            print_r(json_encode([
                'name_surname' => $row['name_surname'],
                'gsm' => $row['gsm']
            ]));
        }
    }elseif($_POST['type'] == 'user-edit')
    {
        $result = $adapter->query('UPDATE personal SET name_surname="'.$_POST['name_surname'].'", gsm="'.$_POST['gsm'].'" WHERE id='.$_POST['id']);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }elseif($_POST['type'] == 'user-add')
    {
        $query = $adapter->prepare("INSERT INTO personal SET
name_surname = ?,
gsm = ?,
create_date = ?");
        $result = $query->execute([
            $_POST['name_surname'],
            $_POST['gsm'],
            date('Y-m-d'),
        ]);

        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }


    if($_POST['type'] == 'action-find')
    {
        $dataList = [];
        $query = $adapter->query("SELECT * FROM actions WHERE id=".$_POST['id'], PDO::FETCH_ASSOC);
        if ( $query->rowCount() ){
            foreach( $query as $row ){
                $dataList = [
                    'note' => $row['note']
                ];
            }
        }

        print_r(json_encode($dataList));
    }

    if($_POST['type'] == 'action-save')
    {
        $result = $adapter->query('UPDATE actions SET status="'.$_POST['status'].'" WHERE id='.$_POST['id']);
        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
    }
}