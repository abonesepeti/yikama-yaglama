<?php
header('Content-Type: application/json');

include 'system/System.php';

$personalList = [];
$query = $adapter->query("SELECT * FROM personal", PDO::FETCH_ASSOC);
if ( $query->rowCount() ){
    foreach( $query as $row ){
        $personalList[$row['id']] = $row['name_surname'];
    }
}

$customerList = [];
$query = $adapter->query("SELECT * FROM customer", PDO::FETCH_ASSOC);
if ( $query->rowCount() ){
    foreach( $query as $row ){
        $customerList[$row['id']] = [
            'name_surname' => $row['name_surname'],
            'plaka' => $row['plaka']
        ];
    }
}

$dataList = [];
$query = $adapter->query("SELECT * FROM actions", PDO::FETCH_ASSOC);
if ( $query->rowCount() ){
    foreach( $query as $row ){
        if($row['status'] == 'Y' || $row['status'] == 'P')
        {
            $dataList['aaData'][] = [
                $customerList[$row['customer_id']]['name_surname'].'    <i class="fa fa-question-circle"></i>',
                $customerList[$row['customer_id']]['plaka'],
                $personalList[$row['personal_id']],
                $row['status'] == 'Y' ? '<span class="badge badge-success">Yıkanıyor</span>' : '<span class="badge badge-warning">Beklemede</span>',
                $row['date'],
                $row['id']
            ];
        }
    }
}

print_r(json_encode($dataList));