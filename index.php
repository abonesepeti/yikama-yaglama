<?php include 'layout/header.php'; ?>
<?php include 'layout/navbar.php'; ?>
<script>
    $(document).ready(function () {
        var  table = $('#datatable').DataTable({
            'ajax' : 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=action',
            'language' : {
                'url' : '//cdn.datatables.net/plug-ins/1.10.19/i18n/Turkish.json'
            },
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": '<div class="btn-group"><button type="button" class="btn btn-info" data-type="note"><i class="fa fa-eye"></i> Not Oku</button><button type="button" class="btn btn-primary" data-type="action"><i class="fa fa-plug"></i> İşlem Yap</button></div>'
            }]
        });


        $('#datatable tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();

            if ($(this).data('type') == 'note') {
                $.ajax({
                    url: '/ajax.php?page=post',
                    type: 'POST',
                    data: {type:'action-find',id:data[5]},
                    success: function (result) {
                        alert(result['note']);
                    }
                })
            }

            if ($(this).data('type') == 'action') {
                var status = "";

                switch ($(data[3]).text()) {
                    case 'Beklemede':
                        status = 'Y';
                        break;
                    case 'Yıkanıyor':
                        status = 'C';
                        break;
                }
                
                $.ajax({
                    url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=post',
                    type: 'POST',
                    data: {type:'action-save',id:data[5],status:status},
                    success: function (result) {
                        table.ajax.reload();
                    }
                })
            }
        });
    });
</script>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-12"><h4 class="page-title m-0">Ana Sayfa</h4></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary mini-stat text-white">
                        <div class="p-3 mini-stat-desc">
                            <div class="clearfix"><h6 class="text-uppercase mt-0 float-left text-white-50">Bekleyen Yıkama</h6>
                                <h4 class="mb-3 mt-0 float-right"><?=$pendingAction;?></h4></div>
                            <div>
                                <span class="ml-2">Sırada olan araç sayısı</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-info mini-stat text-white">
                        <div class="p-3 mini-stat-desc">
                            <div class="clearfix"><h6 class="text-uppercase mt-0 float-left text-white-50">Yıkanan Araç</h6>
                                <h4 class="mb-3 mt-0 float-right"><?=$successAction;?></h4></div>
                            <div>
                                <span class="ml-2">Bugün yıkanan araç sayısı</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-pink mini-stat text-white">
                        <div class="p-3 mini-stat-desc">
                            <div class="clearfix"><h6 class="text-uppercase mt-0 float-left text-white-50">Günlük Kazanç</h6>
                                <h4 class="mb-3 mt-0 float-right"><?=$totalPrice == null ? '0.00' : $totalPrice;?> TL</h4></div>
                            <div>
                                <span class="ml-2">Bugün kazandığımız tutar</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-success mini-stat text-white">
                        <div class="p-3 mini-stat-desc">
                            <div class="clearfix"><h6 class="text-uppercase mt-0 float-left text-white-50">Toplam Müşteri</h6>
                                <h4 class="mb-3 mt-0 float-right"><?=$customerAction;?></h4></div>
                            <div>
                                <span class="ml-2">Sistemte kayıtlı müşteriler</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Müşteri Adı Soyadı</th>
                                    <th>Plaka</th>
                                    <th>Personel</th>
                                    <th>Durumu</th>
                                    <th>Oluşturma Tarihi</th>
                                    <th>İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'layout/footer.php'; ?>