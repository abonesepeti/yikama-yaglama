<?php
try {
    $adapter = new PDO("mysql:host=localhost;dbname=methayikama;charset=utf8", "root", "");
} catch ( PDOException $e ){
    print $e->getMessage();
}

// Bekleyen Araç Sayısı
$pendingActionQuery = $adapter->query("SELECT * FROM actions WHERE status='P' AND date='".date('Y-m-d')."'", PDO::FETCH_ASSOC);
$pendingAction = $pendingActionQuery->rowCount();

// Yıkanan Araç Sayısı
$successActionQuery = $adapter->query("SELECT * FROM actions WHERE status='C' AND date='".date('Y-m-d')."'", PDO::FETCH_ASSOC);
$successAction = $successActionQuery->rowCount();

// Toplam Müşteri Sayısı
$customerActionQuery = $adapter->query("SELECT * FROM customer", PDO::FETCH_ASSOC);
$customerAction = $customerActionQuery->rowCount();

// Toplam Günlük Kazanç
$totalPriceQuery = $adapter->query("SELECT * FROM actions WHERE status='C' AND date='".date('Y-m-d')."'", PDO::FETCH_ASSOC);
$totalPrice = null;

if($totalPriceQuery->rowCount())
{
    foreach( $totalPriceQuery as $row ){
        $totalPrice = $totalPrice + $row['price'];
    }
}

$newUserQuery = $adapter->query('SELECT id FROM customer WHERE create_date="'.date('Y-m-d').'"');
$newUser = $newUserQuery->rowCount();

$customerHomeList = [];
$customerHomeQuery = $adapter->query("SELECT * FROM customer", PDO::FETCH_ASSOC);

foreach( $customerHomeQuery as $row ){
    $customerHomeList[$row['id']] = $row['name_surname'];
}

$personalHomeList = [];
$personalHomeQuery = $adapter->query("SELECT * FROM personal", PDO::FETCH_ASSOC);

foreach( $personalHomeQuery as $row ){
    $personalHomeList[$row['id']] = $row['name_surname'];
}

$hakedisList = [];
$hakedisListQuery = $adapter->query("SELECT * FROM actions WHERE date='".date('Y-m-d')."' AND payment='N'", PDO::FETCH_ASSOC);

foreach( $hakedisListQuery as $row ){
    $hakedisList[$row['personal_id']][] = [
        'price' => $row['price'],
        'percent' => $row['percent'],
        'id' => $row['id']
    ];
}