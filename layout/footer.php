<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">© 2019 Yıkama Yağlama Otomasyon Sistemi | <span class="d-none d-md-inline-block">by <a href="http://mehmethakkioglu.com/">Mehmet HAKKIOĞLU</a></span></div>
        </div>
    </div>
</footer><!-- End Footer --><!-- jQuery  -->
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/bootstrap.bundle.min.js"></script>
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/modernizr.min.js"></script>
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/waves.js"></script>
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/jquery.slimscroll.js"></script><!-- App js -->
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/app.js"></script>
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/jquery.dataTables.min.js"></script>
<script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/dataTables.bootstrap4.min.js"></script>
</body>
</html>