<div class="header-bg"><!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid"><!-- Logo-->
                <div><a href="/index.php" class="logo"><img src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/images/logo.png" alt="" height="26"></a></div>
                <!-- End Logo-->
                <div class="menu-extras topbar-custom navbar p-0">
                    <ul class="list-inline d-none d-lg-block mb-0">
                        <li class="list-inline-item notification-list"><a data-toggle="modal" data-target=".create-action" class="nav-link waves-effect"><i class="fa fa-plus"></i> Yıkama Oluştur</a></li>
                    </ul><!-- Search input -->
                </div><!-- end menu-extras -->
                <div class="clearfix"></div>
            </div><!-- end container --></div><!-- end topbar-main --><!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation"><!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li class="has-submenu"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/index.php"><i class="fa fa-home"></i> Ana Sayfa</a></li>
                        <li class="has-submenu"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/customer.php"><i class="fa fa-car"></i> Müşteri Yönetimi</a></li>
                        <li class="has-submenu"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/user.php"><i class="fa fa-user"></i> Personel Yönetimi</a></li>
                        <li class="has-submenu"><a href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/report.php"><i class="fa fa-chart-area"></i> Rapor Yönetimi</a></li>
                    </ul>
                </div>
                </div>
        </div>
    </header>
</div>