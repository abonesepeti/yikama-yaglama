<?php include "system/System.php"; ?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Yıkama Yağlama Otomasyon Sistemi | Mehmet HAKKIOĞLU</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="ThemeDesign" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->
    <link rel="shortcut icon" href="http://<?=$_SERVER['HTTP_HOST'];?>/assets/images/favicon.ico"><!-- App css -->
    <link href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">

    <script src="http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/assets/js/jquery.min.js"></script>
</head>
<body>
<script>
    $(document).ready(function () {
        $('[data-type="successSave"]').on('click', function () {
            $.ajax({
                url: 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=action-save',
                type: 'POST',
                data: {personal_id:$('[name="personal_id"]').val(),customer_id:$('[name="customer_id"]').val(),price:$('[name="price"]').val(),percent:$('[name="percent"]').val(),note:$('[name="note"]').val()},
                success: function (result) {
                    if(result['code'] == 200)
                    {
                        $('.create-action').modal('toggle');
                        table.ajax.reload();
                    }
                }
            })
        });
    });
</script>
<div class="modal fade create-action" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Yıkama Oluştur</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Müşteri</label>
                    <select name="customer_id" class="form-control">
                        <?php foreach ($customerHomeList as $key => $item): ?>
                        <option value="<?=$key;?>"><?=$item;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Personel</label>
                    <select name="personal_id" class="form-control">
                        <?php foreach ($personalHomeList as $key => $item): ?>
                        <option value="<?=$key;?>"><?=$item;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Fiyat</label>
                    <input type="text" name="price" class="form-control">
                </div>
                <div class="form-group">
                    <label>Yüzde</label>
                    <input type="text" name="percent" class="form-control">
                </div>
                <div class="form-group">
                    <label>Not</label>
                    <textarea name="note" class="form-control" rows="7"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-type="successSave">Oluştur</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Vazgeç</button>
            </div>
        </div>
    </div>
</div>