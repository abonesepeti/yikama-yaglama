<?php include 'layout/header.php'; ?>
<?php include 'layout/navbar.php'; ?>
<script>
    $(document).ready(function () {
        $('[data-type="payment"]').on('click', function () {
            var id = $(this).data('id');

            $.ajax({
                url : 'http://<?=$_SERVER['HTTP_HOST'];?>/yikama-yaglama/ajax.php?page=payment',
                type: 'POST',
                data: {id:id},
                success: function (result) {

                }
            })
        });
    });
</script>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-12"><h4 class="page-title m-0">Rapor Yönetimi</h4></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <div class="card m-b-30">
                        <div class="card-body"><h4 class="mt-0 header-title">Günlük Rakamsal Veriler</h4>
                            <p class="text-muted m-b-30 d-inline-block text-truncate w-100">Bugün yapılan tüm işlemlerin
                                raporları rakamsal olarak aşağıdadır.</p>
                            <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                                <li class="list-inline-item"><h5><?=$successAction;?></h5>
                                    <p class="text-muted">Toplam Yıkama</p></li>
                                <li class="list-inline-item"><h5><?=$totalPrice == null ? '0.00' : $totalPrice;?> TL</h5>
                                    <p class="text-muted">Toplam Kazanç</p></li>
                                <li class="list-inline-item"><h5><?=$newUser;?></h5>
                                    <p class="text-muted">Yeni Müşteri</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="card m-b-30">
                        <div class="card-body"><h4 class="mt-0 header-title">Günlük Hakedişler</h4>
                            <p class="text-muted m-b-30 d-inline-block text-truncate w-100">Hangi personel ne kadar
                                kazanacak?</p>

                            <div class="table-responsive">
                                <table class="table mb-0">
                                    <thead class="thead-default">
                                    <tr>
                                        <th>Adı Soyadı</th>
                                        <th>İşlem Sayısı</th>
                                        <th>Hakediş Ücreti</th>
                                        <th>İşlem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($hakedisList as $key => $item):?>
                                    <?php
                                        $price = 0;
                                        foreach ($item as $prices)
                                        {
                                            $calc = $prices['price'] / 100 * $prices['percent'];
                                            $price = $price + $calc;
                                        }
                                        ?>
                                    <tr>
                                        <td><b><?=$personalHomeList[$key];?></b></td>
                                        <td><?=count($item);?></td>
                                        <td><b><?=$price;?> TL</b></td>
                                        <td>
                                            <button type="button" class="btn btn-success" data-type="payment" data-id="<?=$key;?>"><i class="fa fa-money-bill"></i>  Ödeme Yap</button>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'layout/footer.php'; ?>